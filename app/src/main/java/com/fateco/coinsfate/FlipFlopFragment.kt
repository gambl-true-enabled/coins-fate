package com.fateco.coinsfate

import android.app.AlertDialog
import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.flip_flop_fragment.*
import kotlin.random.Random

class FlipFlopFragment : Fragment() {

    private var isAnim = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.flip_flop_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tap_fragment.setOnClickListener {
            isAnim = !isAnim
            val mp = MediaPlayer.create(context!!, R.raw.cell_phone_flip_1)
            mp.start()
            if (isAnim) {
                Glide
                    .with(context!!)
                    .load(R.drawable.anim_coin)
                    .into(money_coin)
                text_tap.text = "Tap to catch"
            } else {
                text_tap.text = "Tap to toss"
                if (Random.nextBoolean()) {
                    Glide
                        .with(context!!)
                        .load(R.drawable.flip)
                        .into(money_coin)
                } else {
                    Glide
                        .with(context!!)
                        .load(R.drawable.flop)
                        .into(money_coin)
                }
            }
        }

        info1.setOnClickListener {
            val aboutDialog: AlertDialog =
                AlertDialog.Builder(context!!).setMessage("Tap for toss or catch money")
                    .setPositiveButton("OK") { dialog, _ ->
                        dialog.dismiss()
                    }.create()

            aboutDialog.show()
        }
        info2.setOnClickListener {
            (activity as CoinActivity).openInfoFragment()
        }
    }

}
